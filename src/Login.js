import React, { useContext, useState } from "react";
import "./login.css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { LoginContext } from "./contexts/LoginContext";

const schema = yup.object().shape({
  username: yup.string().required("Username Required"),
  email: yup.string().email().required("Email Required"),
  password: yup
    .string()
    .required("Password Required")
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      "Password must contain at least 8 characters, one uppercase, one number and one special case character"
    ),
  confirmPassword: yup
    .string()
    .required("Confirmation Password Required")
    .oneOf([yup.ref("password"), null], "Passwords does not match"),
});

function Login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const [submitting, setSubmitting] = useState(false);

  const submitForm = (data) => {
    console.log(data);
  };

  const { setUsername, setShowTodo } = useContext(LoginContext);

  return (
    <>
      <h1>TO-DO LIST</h1>
      <div className='formContainer'>
        <div className='formContent'>
          <form
            onSubmit={handleSubmit((submitForm) => {
              setSubmitting(true);
              setShowTodo(true);
            })}
            className='form'>
            <h2>Sign Up</h2>
            <div className='formInputs'>
              <label htmlFor='username' className='formLabel'>
                Username
              </label>
              <input
                type='text'
                name='username'
                {...register("username")}
                className='formInput'
                placeholder='Enter your Username'
                onChange={(e) => setUsername(e.target.value)}
              />
              <p> {errors.username?.message} </p>
            </div>
            <div className='formInputs'>
              <label htmlFor='username' className='formLabel'>
                Email
              </label>
              <input
                type='text'
                name='email'
                className='formInput'
                placeholder='Enter your Email'
                {...register("email")}
              />
              <p> {errors.email?.message} </p>
            </div>
            <div className='formInputs'>
              <label htmlFor='username' className='formLabel'>
                Password
              </label>
              <input
                type='password'
                name='password'
                className='formInput'
                placeholder='Enter your password'
                {...register("password")}
              />
              <p> {errors.password?.message} </p>
            </div>
            <div className='formInputs'>
              <label htmlFor='username' className='formLabel'>
                Confirm Password
              </label>
              <input
                type='password'
                name='confirmPassword'
                className='formInput'
                placeholder='Confirm your password'
                {...register("confirmPassword")}
              />
              <p>{errors.confirmPassword?.message}</p>
              {console.log(errors)}
            </div>
            <button
              className='formInputButton'
              type='submit'
              disabled={submitting}>
              {console.log(Object.keys(errors).length)}
              Sign Up
            </button>
          </form>
        </div>
      </div>
    </>
  );
}

export default Login;
