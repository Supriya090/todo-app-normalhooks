import React, { useState } from "react";
import Login from "./Login";
import Todo from "./Todo";
import "./todo.css";
import "./login.css";
import { LoginContext } from "./contexts/LoginContext";

function App() {
  const [username, setUsername] = useState("");
  const [showTodo, setShowTodo] = useState(false);
  const providerValue = {
    username,
    setUsername,
    setShowTodo,
  };

  return (
    <div>
      <LoginContext.Provider value={providerValue}>
        {showTodo ? <Todo /> : <Login />}
      </LoginContext.Provider>
    </div>
  );
}

export default App;
