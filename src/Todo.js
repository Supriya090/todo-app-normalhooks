import React, { useState, useEffect, useContext } from "react";
import Form from "./components/Form";
import { LoginContext } from "./contexts/LoginContext";
import "./todo.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faUser } from "@fortawesome/free-solid-svg-icons";
import { v1 as uuid } from "uuid";

function Todo() {
  const [todos, setTodos] = useState([]);
  const { username } = useContext(LoginContext);

  useEffect(() => {
    const todos = JSON.parse(localStorage.getItem("todos"));
    if (todos) {
      setTodos(todos);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  const onComplete = (id) =>
    setTodos(
      todos.map((todo) =>
        todo.id === id
          ? {
              ...todo,
              complete: !todo.complete,
            }
          : todo
      )
    );

  const removeItems = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
    console.log("Function: " + id);
  };

  return (
    <>
      <div className='todoH1'>
        <h1>TO-DO LIST</h1>
        <div className='avatar'>
          <FontAwesomeIcon icon={faUser} />
          <span className='userName'>{username}</span>
        </div>
      </div>
      <div className='todoClass'>
        <h2 className='todoHead'> Hey! What are your plans today?</h2>
        <Form
          onSubmit={(text) =>
            setTodos([{ text, id: uuid(), complete: false }, ...todos])
          }
        />
        <div className='todoContainer'>
          {todos.map(({ text, id, complete }) => (
            <div key={id} className='todoRow'>
              <div
                style={{
                  textDecoration: complete ? "line-through" : "",
                  color: complete ? "black" : "",
                }}>
                {text}
              </div>
              <div className='icons'>
                <FontAwesomeIcon
                  icon={faTrash}
                  onClick={() => removeItems(id)}
                />
                <label class='container'>
                  <input
                    type='checkbox'
                    onClick={() => onComplete(id)}
                    checked={complete ? "checked" : ""}
                  />
                  <span class='checkmark'></span>
                </label>
              </div>
            </div>
          ))}
        </div>
        {!todos.length ? (
          <h3>Wohoo! No tasks remaining!</h3>
        ) : todos.length === 1 ? (
          <h3>Only one task to go!</h3>
        ) : (
          <h3>{todos.length} tasks remaining!</h3>
        )}
        <button onClick={() => setTodos([])} className='clear' style={{}}>
          Clear
        </button>
      </div>
    </>
  );
}

export default Todo;
